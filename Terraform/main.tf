# Providing the AWS Credentials
provider "aws" {
    profile    = "default"
    region     = var.region
    version    = "~> 2.12.0"
}

# Creating VPC
resource "aws_vpc" "My_VPC" {
    cidr_block           = var.vpcCIDRBlock
    instance_tenancy     = var.instanceTenancy
    enable_dns_support   = var.dnsSupport
    enable_dns_hostnames = var.dnsHostNames

    tags = {
        Name = var.vpcName
    }
}

# Creating IGW
resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.My_VPC.id

  tags = {
      Name = var.InternetGateWay
  }
}

# Creating Public Subnets
resource "aws_subnet" "Public-Subnet-1" {
    vpc_id                  = aws_vpc.My_VPC.id
    cidr_block              = var.Public-Subnet-CIDR-1
    map_public_ip_on_launch = var.mapPublicIP

    tags = {
        Name = var.Public_Subnet_Name-1
    }

    availability_zone = data.aws_availability_zones.available.names[0]
}

# Creating NAT Gateway
resource "aws_nat_gateway" "NAT" {
  allocation_id = var.elasticIP
  subnet_id     = aws_subnet.Public-Subnet-1.id
  depends_on    = [aws_internet_gateway.IGW]
}

# Using Data Source for retrieving AZs
data "aws_availability_zones" "available" {
  state = "available"
}

# Security-Group
resource "aws_security_group" "application_sg" {
  vpc_id      = aws_vpc.My_VPC.id
  name        = "application_sg"
  description = "This Security Group for Application Server"

# Ingress Security Port 80
  ingress {
  cidr_blocks       = var.ingressCIDRBlock
  from_port         = var.ingress_http_from_Port
  to_port           = var.ingress_http_to_Port
  protocol          = var.ingressProtocol
 }

# Ingress Security Port 22
  ingress {
  cidr_blocks = var.ingress_ssh_cidr
  from_port   = var.ingress_SSH_from_Port
  to_port     = var.ingress_SSH_to_Port
  protocol    = var.protocol
 }
# All OutBound access
  egress {
  cidr_blocks       = var.egressCIDRBlock
  from_port         = var.egress_from_port
  to_port           = var.egress_to_port
  protocol          = var.egressProtocol
 }
}