# Variables for AWS Credentials
variable "region" {
  type        = string
  default     = "ap-south-1"
  description = "This is REGION for our Infrastructure"
}

# Variables for VPC
variable "vpcCIDRBlock" {
  type        = string
  default     = "10.0.0.0/16"
  description = "This is the CIDR Block for VPC"
}
variable "instanceTenancy" {
  type        = string
  default     = "default"
  description = "Taking Instance Tenancy"
}
variable "dnsSupport" {
  type        = bool
  default     = true
  description = "Want to take DNS Support"
}
variable "dnsHostNames" {
  type        = bool
  default     = true
  description = "Want to take DNS HostName"
}
variable "vpcName" {
  type        = string
  default     = "cfast-vpc"
  description = "This is your VPC name"
}

# Variables for Internet Gateway
variable "InternetGateWay" {
  type        = string
  default     = "cfast-IGW"
  description = "This is my Internet GateWay name"
}

# Variables for Public Subnets
variable "Public-Subnet-CIDR-1" {
    type        = string
    default     = "10.0.0.0/28"
    description = "The CIDR for PublicSubnet-1"
}
variable "Public_Subnet_Name-1" {
    type        = string
    default     = "cfastPublicSubnet-1"
    description = "Name of our Public Subnet-1"
}
variable "mapPublicIP" {
    type    = bool
    default = true
}

# Variables for NAT Gateway
variable "elasticIP" {
  default     = "eipalloc-040a1bf771b0686e8"
  description = "This is ElasticIP will bound with NAT"
}

# Variables for Security Group
# Variables for Port 80
variable "ingressCIDRBlock" {
    type    = list
    default = [ "0.0.0.0/0" ]
}
variable "ingress_http_from_Port" {
    default = "80"
}
variable "ingress_http_to_Port" {
    default = "80"
}
variable "ingressProtocol" {
    default = "tcp"
}

# Variables for Port 22
variable "ingress_ssh_cidr" {
    default = [ "0.0.0.0/0" ]
}
variable "ingress_SSH_from_Port" {
    default = "22"
}
variable "ingress_SSH_to_Port" {
    default = "22"
}
variable "protocol" {
    default = "tcp"
}

# Variables for Outbound Rules
variable "egressCIDRBlock" {
    type    = list
    default = [ "0.0.0.0/0" ]
}
variable "egress_from_port" {
    default = "0"
}
variable "egress_to_port" {
    default = "0"
}
variable "egressProtocol" {
    default = "-1"
}